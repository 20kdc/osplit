#ifndef OSPLIT_OUT_H
#define OSPLIT_OUT_H
#include "osplit_toc.h"

void osplit_output_comment(const char * held_track_name, int track_no);
void osplit_output(osplit_toc_t * toc, const char * held_track_filename, size_t start, size_t end);

#endif
