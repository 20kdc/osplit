# osplit: Interactive lossless\* Opus audio splitting program

osplit splits Opus files without reencoding them.

## Caveats

\* Due to how Opus streams handle prediction, the very start of an Opus stream may be subject to error as a result of this splitting process. This is unlikely to have an effect on the target use-case, where usually tracks are separated by periods of silence sufficient to allow the codec to re-adjust.

It is not particularly well-tested on any wide range of files and relies heavily on Opus being a "single-configuration" codec.

In general it should be expected that anything other than a single Opus stream or chained set of streams with no overlap or any oddities whatsoever (in other words, concatenated Opus files such as if osplit output was reassembled) will cause the program to fail in some way or another.

## Usage

osplit can either run in interactive mode (`osplit file.opus`) or batch mode (`osplit file.opus -b`).

It outputs resulting files into the current directory.

By default it works in interactive mode.

You'll be asked for timestamps of the rough form `hh:mm:ss`.

Note that numbers do not have to be "in-bounds" - `120:00` is equivalent to `2:00:00` and `7200`.

Here's an example valid style that you might see in practice when copying from a YouTube video description:

```
00:00 - Intro
01:00 - Learning To Shoot
02:34 - Menu
03:45 - The NoU
04:56 - Credits
```

Note that between each timestamp, you will be asked to fine-tune the split point, by entering in amounts of packets to adjust the split point by.

You can play the start of the track by pressing Enter.

Once you are being asked for a timestamp and have no more timestamps to give (i.e. the last timestamp you gave extends to the end of the file, use Control-D (EOF) to finish.

### Batch mode

Batch mode does not allow fine-tuning after accepting timestamps.

As a result, you can copy/paste large amounts of timestamps.

This mode is specifically intended for carelessly copy/pasting YouTube description timestamp lists.

## Dependencies

osplit depends on SDL2 for audio output (I tried PortAudio and got crackling and buffer underruns, which are INCREDIBLY harmful when trying to do precise audio cuts), along with libogg and libopus.

Creating builds for non-Linux operating systems should just be a matter of compiling it, but I have not yet tested this. Just know that it isn't actively doing anything to make it more difficult to compile.

## License

```
Written starting in 2020 by 20kdc
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
```

