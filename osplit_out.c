#include <stdio.h>
#include <ogg/ogg.h>
#include <opus/opus.h>
#include <time.h>

#include "osplit_out.h"
#include "osplit_line.h"

// 512 is overkill, but that's good
static char osplit_out_comment_buf[OSPLIT_LINE_BUF_LEN + 512];
static int osplit_out_comment_len;

static void osplit_output_flush(ogg_stream_state * os, FILE * fio, int force) {
	ogg_page page;
	while (1) {
		int more = 0;
		if (force) {
			more = ogg_stream_flush(os, &page);
		} else {
			more = ogg_stream_pageout(os, &page);
		}
		if (!more)
			break;
		fwrite(page.header, page.header_len, 1, fio);
		fwrite(page.body, page.body_len, 1, fio);
	}
}

static void osplit_output_comment_str(const char * keye, const char * val) {
	size_t keye_len = strlen(keye);
	size_t val_len = strlen(val);
	uint32_t len = (keye_len + val_len);
	((uint32_t *) (osplit_out_comment_buf + osplit_out_comment_len))[0] = len;
	osplit_out_comment_len += 4;
	memcpy(osplit_out_comment_buf + osplit_out_comment_len, keye, keye_len);
	osplit_out_comment_len += keye_len;
	memcpy(osplit_out_comment_buf + osplit_out_comment_len, val, val_len);
	osplit_out_comment_len += val_len;
}

void osplit_output_comment(const char * held_track_name, int track_no) {
	memcpy(osplit_out_comment_buf, "OpusTags", 8);
	osplit_out_comment_len = 8;
	osplit_output_comment_str("MRWH", "");
	osplit_out_comment_buf[osplit_out_comment_len++] = 2;
	for (int i = 0; i < 3; i++)
		osplit_out_comment_buf[osplit_out_comment_len++] = 0;
	osplit_output_comment_str("TITLE=", held_track_name);
	char track_no_str[16];
	sprintf(track_no_str, "%i", track_no);
	osplit_output_comment_str("TRACKNUMBER=", track_no_str);
}

void osplit_output(osplit_toc_t * toc, const char * fn, size_t start, size_t end) {
	FILE * fio = fopen(fn, "wb");
	if (!fio) {
		puts("Failed to open file!");
		return;
	}
	ogg_stream_state os;
	ogg_stream_init(&os, (int) time(NULL));
	ogg_packet pkt = {};
	// write out headers / init pkt
	// ID
	pkt.packet = "OpusHead" "\x01\x02\x00\x00" "\x80\xBB\x00\x00" "\x00\x00\x00";
	pkt.bytes = 19;
	pkt.b_o_s = 1;
	pkt.e_o_s = 0;
	pkt.granulepos = 0;
	pkt.packetno = 0;
	ogg_stream_packetin(&os, &pkt);
	osplit_output_flush(&os, fio, 1);
	// Comment
	pkt.packet = osplit_out_comment_buf;
	pkt.bytes = osplit_out_comment_len;
	pkt.b_o_s = 0;
	pkt.e_o_s = start == end;
	pkt.packetno++;
	ogg_stream_packetin(&os, &pkt);
	osplit_output_flush(&os, fio, 1);
	// write out data
	while (start != end) {
		// write packet
		pkt.packet = toc->packets[start].dat;
		pkt.bytes = toc->packets[start].len;
		if (start == (end - 1))
			pkt.e_o_s = 1;
		pkt.packetno++;
		pkt.granulepos += opus_packet_get_nb_samples(pkt.packet, pkt.bytes, 48000);
		ogg_stream_packetin(&os, &pkt);
		osplit_output_flush(&os, fio, 0);
		start++;
	}
	// shutdown
	osplit_output_flush(&os, fio, 1);
	ogg_stream_clear(&os);
	fclose(fio);
}

