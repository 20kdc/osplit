#ifndef OSPLIT_PLAY_H
#define OSPLIT_PLAY_H
#include "osplit_toc.h"
size_t osplit_play(osplit_toc_t * toc, size_t start, size_t count);
#endif
