osplit: osplit.c osplit_toc.h osplit_toc.c osplit_play.h osplit_play.c osplit_line.h osplit_line.c osplit_out.c osplit_out.h
	$(CC) osplit.c osplit_toc.c osplit_play.c osplit_line.c osplit_out.c -logg -lopus -lSDL2 -o osplit

clean:
	rm -f osplit
