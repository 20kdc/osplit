#ifndef OSPLIT_LINE_H
#define OSPLIT_LINE_H

#define OSPLIT_LINE_BUF_LEN 128

extern char osplit_line_buf[OSPLIT_LINE_BUF_LEN];

// This will return non-zero on failure.
// If it is allowed to be interactive, it is less likely to fail.
int osplit_line(int interactive);
// Splits a timestamp/title line: "05:00 - AB" or "05:00 AB"
// Returns NULL and does no harm if impossible to split. Otherwise returns title area.
char * osplit_line_split_ts();

// Returns -1 if the timestamp can't be parsed.
int osplit_line_ts();

// If non-zero, then val is valid
int osplit_line_num(int * val);

#endif
