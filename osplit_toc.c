#include "osplit_toc.h"

// Reads in file and builds page TOC. Notably, this is the only "active" use of the Ogg sync system.
osplit_toc_t * osplit_toc_new(const char * srcn) {
	osplit_toc_t * toc = malloc(sizeof(osplit_toc_t));
	if (!toc) {
		puts("Could not malloc TOC");
		return NULL;
	}
	FILE * source = fopen(srcn, "rb");
	if (!source) {
		puts("Could not open file");
		free(toc);
		return NULL;
	}
	toc->packets = NULL;
	toc->count = 0;
	// Begin reading...
	ogg_sync_state syn;
	ogg_sync_init(&syn);
	ogg_stream_state stm;
	ogg_stream_init(&stm, 0);
	int packet_choke = 0;
	ogg_int64_t time = 0;
	ogg_int64_t time_last_page = 0;
	while (!feof(source)) {
		char * buf = ogg_sync_buffer(&syn, 4096);
		size_t bytes = fread(buf, 1, 4096, source);
		if (ogg_sync_wrote(&syn, bytes))
			break;
		ogg_page page;
		while (ogg_sync_pageout(&syn, &page) == 1) {
			if (ogg_page_bos(&page)) {
				int sn = ogg_page_serialno(&page);
				printf("Stream ID: %08x\n", sn);
				ogg_stream_reset_serialno(&stm, sn);
				packet_choke = 2;
			}
			ogg_stream_pagein(&stm, &page);
			ogg_packet op;
			while (1) {
				int st = ogg_stream_packetout(&stm, &op);
				if (st == 0)
					break;
				if (st == 1) {
					if (packet_choke) {
						packet_choke--;
						continue;
					}
					// Audio packet!
					char * dat = malloc(op.bytes);
					if (dat) {
						memcpy(dat, op.packet, op.bytes);
						toc->count++;
						toc->packets = realloc(toc->packets, toc->count * sizeof(osplit_toc_entry_t));
						osplit_toc_entry_t * ent = toc->packets + (toc->count - 1);
						ent->start = time;
						ent->dat = dat;
						ent->len = op.bytes;
					}
				}
			}
			// Update time
			if (!ogg_page_bos(&page))
				time += ogg_page_granulepos(&page) - time_last_page;
			time_last_page = ogg_page_granulepos(&page);
		}
	}
	ogg_stream_clear(&stm);
	ogg_sync_clear(&syn);
	fclose(source);
	return toc;
}

int osplit_toc_ts_to_idx(osplit_toc_t * toc, int ts, size_t * ptr) {
	if (ts == 0) {
		*ptr = 0;
		return 0;
	}
	ogg_int64_t sample = ts * 48000;
	for (size_t i = 0; i < toc->count; i++) {
		if (toc->packets[i].start >= sample) {
			*ptr = i;
			return 0;
		}
	}
	return 1;
}

void osplit_toc_free(osplit_toc_t * toc) {
	for (size_t i = 0; i < toc->count; i++)
		free(toc->packets[i].dat);
	free(toc->packets);
	free(toc);
}

