#include <stdio.h>
#include <SDL2/SDL.h>
#include <opus/opus.h>
#include "osplit_play.h"

#define OSPLIT_PLAY_BUF 5760

typedef struct {
	opus_int16 left;
	opus_int16 right;
} osplit_frame_t;

typedef struct {
	OpusDecoder * opus;
	osplit_toc_t * toc;
	size_t start;
	size_t count;
	osplit_frame_t buffer[OSPLIT_PLAY_BUF];
	size_t buffer_pos;
	size_t buffer_len;
} osplit_play_pacb_t;

static void osplit_play_pacb(osplit_play_pacb_t * state, osplit_frame_t * output, int len) {
	unsigned long count = (unsigned long) (len / sizeof(osplit_frame_t));
	for (unsigned long i = 0; i < count; i++) {
		if (state->buffer_pos == state->buffer_len) {
			if (state->count != 0) {
				// Pull packet entry
				osplit_toc_entry_t * ent = state->toc->packets + state->start;
				state->count--;
				state->start++;
				// Setup buffer state
				state->buffer_pos = 0;
				state->buffer_len = opus_decode(state->opus, ent->dat, ent->len, (opus_int16 *) state->buffer, OSPLIT_PLAY_BUF, 0);
			}
		}
		if (state->buffer_pos == state->buffer_len) {
			// Failed
			output[i].left = 0;
			output[i].right = 0;
		} else {
			output[i] = state->buffer[state->buffer_pos++];
		}
	}
}

size_t osplit_play(osplit_toc_t * toc, size_t start, size_t count) {
	osplit_play_pacb_t data;
	int error;
	data.opus = opus_decoder_create(48000, 2, &error);
	data.toc = toc;
	data.start = start;
	data.count = count;
	data.buffer_pos = 0;
	data.buffer_len = 0;
	SDL_AudioSpec desired;
	desired.freq = 48000;
	desired.format = AUDIO_S16;
	desired.channels = 2;
	desired.samples = 4096;
	desired.callback = (SDL_AudioCallback) osplit_play_pacb;
	desired.userdata = &data;
	if (SDL_OpenAudio(&desired, NULL)) {
		printf("-- failed to play! --");
		opus_decoder_destroy(data.opus);
		return data.start;
	}
	SDL_PauseAudio(0);
	printf("playing, press enter to stop...");
	while (getchar() != 10);
	SDL_CloseAudio();
	opus_decoder_destroy(data.opus);
	return data.start;
}
