#include <string.h>
#include <stdio.h>
#include "osplit_line.h"

#define OSPLIT_LINE_BUF_LEN 128

char osplit_line_buf[OSPLIT_LINE_BUF_LEN];

int osplit_line(int interactive) {
	int ok = 0;
	while (!ok) {
		if (feof(stdin))
			return 1;
		ok = 1;
		fgets(osplit_line_buf, OSPLIT_LINE_BUF_LEN, stdin);
		while (strlen(osplit_line_buf) == (OSPLIT_LINE_BUF_LEN - 1)) {
			ok = 0;
			fgets(osplit_line_buf, OSPLIT_LINE_BUF_LEN, stdin);
		}
		if (!ok) {
			printf("Too long!: ");
			if (!interactive)
				return 1;
		}
	}
	while (1) {
		size_t len = strlen(osplit_line_buf);
		if (len == 0)
			break;
		char * str = osplit_line_buf + len - 1;
		if ((*str == 10) || (*str == 13)) {
			*str = 0;
		} else {
			break;
		}
	}
	return 0;
}

char * osplit_line_split_ts() {
	size_t len = strlen(osplit_line_buf);
	int state = 0;
	for (size_t i = 0; i < len; i++) {
		if ((osplit_line_buf[i] == ' ') || (osplit_line_buf[i] == '-')) {
			osplit_line_buf[i] = 0;
			state = 1;
		} else if (state) {
			return osplit_line_buf + i;
		}
	}
	return NULL;
}

int osplit_line_ts() {
	char * st = osplit_line_buf;
	int ts = 0;
	int acc = 0;
	while (*st) {
		char ch = *st;
		if ((ch >= '0') && (ch <= '9')) {
			ts *= 10;
			ts += ch - '0';
		} else if (ch == ':') {
			// clear field and advance
			acc += ts;
			acc *= 60;
			ts = 0;
		} else {
			return -1;
		}
		st++;
	}
	return ts + acc;
}

int osplit_line_num(int * val) {
	char * st = osplit_line_buf;
	int ts = 0;
	int mul = 1;
	while (*st) {
		char ch = *st;
		if ((ch >= '0') && (ch <= '9')) {
			ts *= 10;
			ts += ch - '0';
		} else if (ch == '-') {
			mul = mul * -1;
		} else {
			return 0;
		}
		st++;
	}
	*val = ts * mul;
	return 1;
}
