#include <stdio.h>
#include <SDL2/SDL.h>

#include "osplit_toc.h"
#include "osplit_play.h"
#include "osplit_line.h"
#include "osplit_out.h"

static char held_track_name[OSPLIT_LINE_BUF_LEN];
static char held_track_filename[OSPLIT_LINE_BUF_LEN + 5];
static char in_track_name[OSPLIT_LINE_BUF_LEN];

static void main_body(osplit_toc_t * toc, int interactive);

int main(int argc, char ** argv) {
	int argsok = 0;
	int interactive = 1;

	if (argc == 2) {
		argsok = 1;
	} else if (argc == 3) {
		if (!strcmp("-b", argv[2])) {
			argsok = 1;
			interactive = 0;
		}
	}
	if (!argsok) {
		puts("osplit test.opus [-b]");
		puts("Splits an Opus file into separate tracks in the current directory.");
		puts("Usually interactive.");
		puts("-b: Batch (read timestamps, i.e. \"05:00 - Supercalm\", and don't ask questions)");
		return 1;
	}
	osplit_toc_t * toc = osplit_toc_new(argv[1]);
	if (!toc) {
		printf("Unable to get packets.\n");
		return 1;
	}
	SDL_Init(SDL_INIT_AUDIO);
	long estlen = 0;
	int status = 0;
	if (toc->count) {
		estlen = toc->packets[toc->count - 1].start / 48000;
		printf("Acquired %li packets. Time around %li seconds.\n", toc->count, estlen);
		main_body(toc, interactive);
	} else {
		puts("Failed to lock on to Opus stream.");
		status = 1;
	}
	puts("Splitting complete!");
	osplit_toc_free(toc);
	SDL_Quit();
	return status;
}

static void main_body(osplit_toc_t * toc, int interactive) {
	int track_no = 1;
	// Initial held track: PRELUDE
	size_t held_track_start = 0;
	strcpy(held_track_name, "PRELUDE");
	strcpy(held_track_filename, "PRELUDE.opus");
	// Track being input
	size_t in_track_start = 0;

	// This state machine uses a lot of goto. Deal with it. It's better for this pattern.
	// Timestamp - Name input
	state_coarse: while (1) {
		if (interactive) {
			if (track_no == 1) {
				printf("Timestamp - Name (00:00 - Test)? ");
			} else {
				if (track_no == 2)
					printf("Note that you can press Ctrl-D here to signal EOF (last track extends to end of file).\n");
				printf("Timestamp - Name (01:23 - Test)? ");
			}
		}
		while (1) {
			if (osplit_line(interactive))
				goto state_end;
			if (!strcmp("", osplit_line_buf)) {
				if (interactive)
					puts("You didn't specify a timestamp. If you're done, press Ctrl-D (send EOF).");
			} else {
				break;
			}
		}
		
		char * name = osplit_line_split_ts();
		int ts = osplit_line_ts();
		if ((ts == -1) || (!name) || osplit_toc_ts_to_idx(toc, ts, &in_track_start)) {
			if (!interactive) {
				puts("Invalid timestamp/name pair:");
				puts(osplit_line_buf);
			} else {
				puts("Invalid timestamp/name.");
			}
			continue;
		}
		// Finally!
		strcpy(in_track_name, name);
		if (interactive) {
			goto state_fine;
		} else {
			goto state_next;
		}
	}
	state_next: {
		if (in_track_start > held_track_start) {
			// Output
			osplit_output_comment(held_track_name, track_no++);
			osplit_output(toc, held_track_filename, held_track_start, in_track_start);
		}
		// Advance
		held_track_start = in_track_start;
		strcpy(held_track_name, in_track_name);
		sprintf(held_track_filename, "%s.opus", held_track_name);
		goto state_coarse;
	}
	state_end: {
		osplit_output_comment(held_track_name, track_no++);
		osplit_output(toc, held_track_filename, held_track_start, toc->count);
		return;
	}
	// Fine adjustment state. This isn't core to the flow, so it's moved down here.
	// But it needs goto access to return to coarse
	state_fine: while (1) {
		printf("Fine tuning: Either a number (offset in packets), '' (play), 'back' (change ts/name), 'help' or 'confirm'.\n");
		while (1) {
			printf("%li - %li - %li? ", held_track_start, in_track_start, toc->count);
			if (osplit_line(interactive))
				goto state_end;
			int val;
			if (!strcmp("", osplit_line_buf)) {
				size_t pl = osplit_play(toc, in_track_start, toc->count - in_track_start);
				printf("Stopped at: %li\n", pl);
			} else if (!strcmp("back", osplit_line_buf)) {
				goto state_coarse;
			} else if (!strcmp("confirm", osplit_line_buf)) {
				goto state_next;
			} else if (!strcmp("help", osplit_line_buf)) {
				break;
			} else if (osplit_line_num(&val)) {
				if (val < 0) {
					val = -val;
					if (val >= in_track_start) {
						printf("Outside of bounds.\n");
					} else {
						in_track_start -= val;
					}
				} else {
					if ((in_track_start + val) >= toc->count) {
						printf("Outside of bounds.\n");
					} else {
						in_track_start += val;
					}
				}
			} else {
				printf("Invalid.\n");
			}
		}
	}
}

