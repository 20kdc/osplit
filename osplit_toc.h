#ifndef OSPLIT_TOC_H
#define OSPLIT_TOC_H
#include <stdio.h>
#include <stdlib.h>
#include <ogg/ogg.h>
#include <string.h>

typedef struct {
	// Estimated start time of this packet. This will tend to be too low.
	ogg_int64_t start;
	char * dat;
	size_t len;
} osplit_toc_entry_t;

typedef struct {
	osplit_toc_entry_t * packets;
	size_t count;
} osplit_toc_t;

// Reads in file. Notably, this is the only "active" use of the Ogg sync system.
// Further seeking is done in-memory (it's audio, just deal with it)
osplit_toc_t * osplit_toc_new(const char * source);
// Finds a packet given a timestamp. Returns 1 on failure.
int osplit_toc_ts_to_idx(osplit_toc_t * toc, int ts, size_t * ptr);
// Frees the TOC.
void osplit_toc_free(osplit_toc_t * toc);
#endif
